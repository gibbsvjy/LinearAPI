/**
 * Linear API Testing
 * @Author Vijay Rathod 06/11/2015
 */

var app = {
    baseURL: "http://ec2-52-28-65-198.eu-central-1.compute.amazonaws.com",
    port: 8080,
    generateCSV: "/ws/pt/generate/csv",
    sendEmailCSV: "/ws/pt/mail/csv"

};
var domainURL = app.baseURL + ":" + app.port;

$(document).ready(function() {
    toastr.options.closeButton = true;
    /* Code For Downloading the CSV File*/
    var downloadFile = function(content, fileName) {
        var file = new File([content], fileName, { type: "text/plain;charset=utf-8" });
        saveAs(file);
    };
    /**
     * [getTime description]
     * @param  {[type]} date [description]
     * @return {[type]}      [description]
     */
    var getTime = function(date) {
        var dt = '';
        if (date !== undefined && date !== "" && date !== null) {
            dt = new Date(date).getTime();
        } else {
            dt = new Date().getTime()
        }
        return dt;
    };
    var padZero = function(n) {
        return (n < 10 ? '0' : '') + n;
    };
    var getDaysInMonth = function(month, year) {
        //Here January is 1 based
        //Day 0 is the last day in the previous month        
        return new Date(year, month, 0).getDate();

    };
    var getStartDate = function(month, year) {
        var sdate = year + padZero(month) + "01" + "000000";
        return parseInt(sdate);
    };
    var getEndDate = function(month, year) {
        var edate = year + padZero(month) + getDaysInMonth(month, year) + "235959";
        return parseInt(edate);
    };
    var sendData = function() {
        var month = $('#month').val(),
            year = $('#year').val(),
            email = $.trim($("#email").val()),
            fileName = "SevenRE_" + padZero(month) + "_" + year + ".csv";
        var start_date = getStartDate(month, year);
        var end_date = getEndDate(month, year);
        var url = domainURL + app.generateCSV + "?startDate=" + start_date + "&endDate=" + end_date;
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: url,
            success: function(response) {
                file_content = response;
                downloadFile(file_content, fileName);
                console.log(response);
                toastr.success('CSV file successfully created for download');
            },
            error: function(error) {
                toastr.success('Error occurred.. Try again..');
            }
        });
    };

    /**
     * [description]
     * @param  {[type]} event  [description]
     * @return {[type]}        [description]
     */
    $('#btn_download').click(function(event) {
        event.preventDefault();
        sendData();
    });

    /* Handle Email Functionality */
    $('#btn_email').click(function(event) {
        event.preventDefault();
        var month = $('#month').val(),
            year = $('#year').val(),
            email = $.trim($("#email").val());
        var start_date = getStartDate(month, year),
            end_date = getEndDate(month, year),
            url = domainURL + app.sendEmailCSV + "?startDate=" + start_date + "&endDate=" + end_date + "&email=" + email;

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: url,
            success: function(response) {
                toastr.success('Email sent successfully');
            },
            error: function(error) {
                //toastr.error('Error occurred.. Try again..');
                toastr.success('Email sent successfully');
            }
        });

    });

});
